"use client";

import { createContext, useContext } from "react";
import { db } from "@/lib/db";
import { currentProfile } from "@/lib/currentProfile";
import { redirectToSignIn } from "@clerk/nextjs";
import { redirect } from "next/navigation";

type SeverType = {
  id?: string;
  name: string;
  imageUrl: string;
  inviteCode: string;
  profileId: string;
  createdAt?: Date;
  updatedAt?: Date;
};

type ProfileType = {
  id?: string;
  userId: string;
  name: string;
  imageUrl: string;
  email: string;
  createdAt?: Date;
  updatedAt?: Date;
};

type SocketContextType = {
  profile: ProfileType | null;
  server: SeverType | null;
};

const ServerContext = createContext<SocketContextType>({
  profile: null,
  server: null,
});

export const useServer = () => {
  return useContext(ServerContext);
};

export const ServerProvider = async ({
  children,
  serverId,
}: {
  children: React.ReactNode;
  serverId: string;
}) => {
  const profile = await currentProfile();

  if (!profile) {
    return redirectToSignIn();
  }

  const server = await db.server.findUnique({
    where: {
      id: serverId,
      members: {
        some: {
          profileId: profile.id,
        },
      },
    },
  });

  if (!server) {
    return redirect("/");
  }

  return (
    <ServerContext.Provider value={{ server, profile }}>
      {children}
    </ServerContext.Provider>
  );
};
